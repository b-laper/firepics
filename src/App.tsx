import { useState } from "react";
import Header from "./components/Header";
import { ImageGrid } from "./components/ImageGrid";
import { ModalPic } from "./components/ModalPic";
import UploadForm from "./components/UploadForm";

const App = (): JSX.Element => {
  const [selectedImg, setSelectedImg] = useState("");
  return (
    <div className="App">
      <Header />
      <UploadForm />
      <ImageGrid setSelectedImg={setSelectedImg} />
      {selectedImg && (
        <ModalPic selectedImg={selectedImg} setSelectedImg={setSelectedImg} />
      )}
    </div>
  );
};

export default App;
