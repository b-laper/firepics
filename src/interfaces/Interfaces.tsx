export interface selectedImgProps {
  selectedImg: string;
}
export interface setSelectedImgProps {
  setSelectedImg: (val: string) => void;
}

export interface ModalPicProps {
  selectedImg: string;
  setSelectedImg: (val: string) => void;
}
