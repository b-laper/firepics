import { useState, useEffect } from "react";
import { projectStorage, projectFirestore } from "../firebaseConfig";
import { doc, setDoc, serverTimestamp } from "firebase/firestore";

import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";

export const useStorage = (file: any) => {
  const [progress, setProgress] = useState<number>(0);
  const [error, setError] = useState<string>("");
  const [url, setUrl] = useState<string>("");
  const date = new Date();
  const id = date.getTime();
  useEffect(() => {
    const storageRef = ref(projectStorage, file.name);
    const collectionRef = doc(projectFirestore, "pictures", `${id}}`);

    const uploadTask = uploadBytesResumable(storageRef, file);
    uploadTask.on(
      "state_changed",
      (snapshot) => {
        let percentage =
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        setProgress(percentage);
      },
      (err: any) => {
        setError(err);
      },
      async () => {
        await getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
          console.log("File available at", downloadURL);
          setUrl(downloadURL);
          const createdAt = serverTimestamp();
          setDoc(collectionRef, { url: downloadURL, createdAt });
        });
      }
    );
  }, [file]);
  return { progress, url, error };
};
