import { useState, useEffect } from "react";
import { projectFirestore } from "../firebaseConfig";
import { collection, query, orderBy, onSnapshot } from "firebase/firestore";

export const useFirestore = (collectionDB: string) => {
  const [docs, setDocs] = useState<Array<Object>>([]);

  useEffect(() => {
    const q = query(
      collection(projectFirestore, "pictures"),
      orderBy("createdAt", "desc")
    );
    const unsubscribe = onSnapshot(q, (querySnapshot) => {
      let documents: Array<Object> = [];
      querySnapshot.forEach((doc) => {
        documents.push({ ...doc.data(), id: doc.id });
      });
      setDocs(documents);
    });
    return () => {
      unsubscribe();
    };
  }, [collectionDB]);

  return { docs };
};
