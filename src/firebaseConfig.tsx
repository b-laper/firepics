import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyDQIRjuH8lLJx-AakKG_ULaqvwsGSj1Mh8",
  authDomain: "firepics-76e93.firebaseapp.com",
  projectId: "firepics-76e93",
  storageBucket: "firepics-76e93.appspot.com",
  messagingSenderId: "697883703686",
  appId: "1:697883703686:web:15cc964183ff68104bd407",
};
const app = initializeApp(firebaseConfig);

const projectStorage = getStorage(app);
const projectFirestore = getFirestore(app);
export { projectFirestore, projectStorage };
