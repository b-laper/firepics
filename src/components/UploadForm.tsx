import React, { useState } from "react";
import { ProgressBar } from "./ProgressBar";

const UploadForm = (): JSX.Element => {
  const [file, setFile] = useState<File | null>(null);
  const [error, setError] = useState<string>("");
  const types = ["image/png", "image/jpeg", "image/jpg"];

  const changeHandeler = (event: React.ChangeEvent<HTMLInputElement>) => {
    let selectedFile = event.target.files?.[0];
    if (selectedFile && types.includes(selectedFile.type)) {
      setFile(selectedFile);
      setError("");
    } else {
      setFile(null);
      setError("Please upload an image file (png, jpg or jpeg)");
    }
  };

  return (
    <form>
      <label>
        <input type="file" onChange={changeHandeler} />
        <span>+</span>
      </label>
      <div className="output">
        {error && <div className="error">{error}</div>}
        {file && <div>{file.name}</div>}
        {file && <ProgressBar file={file} setFile={setFile} />}
      </div>
    </form>
  );
};

export default UploadForm;
