import { ModalPicProps } from "../interfaces/Interfaces";
import { motion } from "framer-motion";

export const ModalPic = ({
  selectedImg,
  setSelectedImg,
}: ModalPicProps): JSX.Element => {
  const handleClick = (event: any) => {
    console.log(event.classList);
    if (event.target.classList.contains("backdrop")) setSelectedImg("");
  };

  return (
    <motion.div
      className="backdrop"
      onClick={handleClick}
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
    >
      <motion.img
        initial={{ y: "-100vh" }}
        animate={{ y: 0 }}
        src={selectedImg}
        alt="selected pic"
      />
    </motion.div>
  );
};
