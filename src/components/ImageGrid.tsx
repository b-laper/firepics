import { useFirestore } from "../hooks/useFirestore";
import { setSelectedImgProps } from "../interfaces/Interfaces";
import { motion } from "framer-motion";

export const ImageGrid = ({
  setSelectedImg,
}: setSelectedImgProps): JSX.Element => {
  const { docs } = useFirestore("pictures");

  console.log(docs);
  return (
    <div className="img-grid">
      {docs &&
        docs.map((doc: any) => (
          <motion.div
            whileHover={{ opacity: 1 }}
            layout
            className="img-wrap"
            key={doc.id}
            onClick={() => setSelectedImg(doc.url)}
          >
            <motion.img
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              transition={{ delay: 0.7 }}
              src={doc.url}
              alt={`pic${doc.id}`}
            />
          </motion.div>
        ))}
    </div>
  );
};
